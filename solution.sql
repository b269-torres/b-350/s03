USE blog_db;

-- user information

INSERT INTO users (email, password, datetime_created) VALUES ("johnsmith@gmail.com", "passwordA", 20210101010000);

INSERT INTO users (email, password, datetime_created) VALUES ("juandelacruz@gmail.com", "passwordb", 20210101020000);

INSERT INTO users (email, password, datetime_created) VALUES ("janesmith@gmail.com", "passwordC", 20210101030000);

INSERT INTO users (email, password, datetime_created) VALUES ("mariadelacruz@gmail.com", "passwordD", 20210101040000);

INSERT INTO users (email, password, datetime_created) VALUES ("johndoe@gmail.com", "passwordE", 20210101050000);

-- Post information

INSERT INTO posts (title, content, datetime_posted, users_id) VALUES ("First Code", "Hello World!", 20210102010000, 1);

INSERT INTO posts (title, content, datetime_posted, users_id) VALUES ("Second Code", "Hello Earth!", 20210102020000, 1);

INSERT INTO posts (title, content, datetime_posted, users_id) VALUES ("Third Code", "Hello to Mars!", 20210102030000, 2);

INSERT INTO posts (title, content, datetime_posted, users_id) VALUES ("Fourth Code", "Bye bye solar system!", 20210102040000, 4);

-- Get all the post with author id of 1

SELECT * FROM posts WHERE users_id = 1;

--Get all the user's email and datetime of creation.

SELECT email, datetime_created FROM users;

--Update a post's content to "Hello to the people of the Earth!” where its initial content is "Hello Earth!" by using the record's ID.

UPDATE posts SET content = "Hello to the people of the Earth!" WHERE content = "Hello Earth!";

--Delete the user with an email of "johndoe@gmail.com".

DELETE FROM users WHERE email = "johndoe@gmail.com";


--stretch goal

USE music_db

INSERT INTO users (username, password, full_name, contact_number, email, address) VALUES ("johndoe", "john1234", "John Doe", 09123456789, "john@gmail.com", "Qeuzon City");

-- Playlist

INSERT INTO playlists (playlist_name, datetime_created, user_id) VALUES ("johndoe's Playlist", 20230820080000, 1);

--Playlist songs

INSERT INTO playlists_songs (playlist_id, song_id) VALUES (1, 1), (1, 2);